<?php

declare(strict_types = 1);

namespace Valonnopea\WordPress;

if (!defined('ABSPATH'))
{
	exit;
}

/**
 * @wordpress-plugin
 * Plugin Name: Valonnopea base plugin
 * Plugin URI: https://gitlab.com/valonnopea/wordpress/
 * Description: Base plugin for Valonnopea WordPress plugins
 * Version: 0.10.2
 * Author: Valonnopea
 * Author URI: https://valonnopea.fi/
 * Developer: Mira Manninen
 * Developer URI: https://github.com/Mireiawen
 * Domain Path: /languages
 *
 * Requires PHP: 8.0
 *
 * License: MIT
 */

/**
 * The base directory path for this plugin
 *
 * @var string
 */
const VALONNOPEA_BASE_PLUGIN_DIR = __DIR__;

require_once(VALONNOPEA_BASE_PLUGIN_DIR . '/vendor/autoload.php');
