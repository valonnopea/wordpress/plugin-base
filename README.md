# WordPress base plugin

Provides some base classes and functionality for Valonnopea WordPress plugins.

Install like any other plugin. This plugin is required by Valonnopea WordPress plugins.
