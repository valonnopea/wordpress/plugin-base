#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

NAME='valonnopea-plugin-base'
PREFIX='Valonnopea'
NAMESPACES_TO_FIX=( 'Carbon_Fields' )

function fix_namespaces()
{
	local namespaces="$( IFS='|' ; echo "${NAMESPACES_TO_FIX[*]}")"
	sed --regexp-extended --in-place \
		"s/^(use[[:space:]]+)(${namespaces})(\\\\)/\1${PREFIX}\\\\\2\3/g" \
		"${SOURCE_DIR}/${1}"
}

function copy_stuff()
{
	rm -rf "${TARGET_DIR}/${1}"
	cp -r "${SOURCE_DIR}/${1}" "${TARGET_DIR}/${1}"
}

# Get the directories to use
TEMPDIR="$(mktemp -d)"
MY_DIR="$(realpath "$(dirname "${0}")")"
SOURCE_DIR="${MY_DIR}/build"
TARGET_DIR="${1%/}/wp-content/mu-plugins/${NAME}"

# Install the composer dependencies
composer --no-interaction --ignore-platform-reqs \
	--working-dir="${MY_DIR}" \
	'install'

composer \
	'bin' \
	'all' 'install' \
	--no-interaction \
	--ignore-platform-reqs

# Run the scoping
"${MY_DIR}/vendor/bin/php-scoper" 'add-prefix' \
	--working-dir "${MY_DIR}" \
	--output-dir "${SOURCE_DIR}" \
	--force \
	--stop-on-failure \
	--no-interaction \
	--prefix "${PREFIX}"

# Fix the autoloader
composer --no-interaction --ignore-platform-reqs \
	--working-dir="${SOURCE_DIR}" \
	'dump-autoload' \
	--classmap-authoritative

# Fix the namespaces
fix_namespaces 'src/AbstractPlugin.php'

# Do the install
mkdir -p "${TARGET_DIR}"
copy_stuff 'vendor'
copy_stuff 'src'

SOURCE_DIR="${MY_DIR}"
copy_stuff "${NAME}.php"

TARGET_DIR="$(dirname "${TARGET_DIR}")"
copy_stuff 'loader.php'
