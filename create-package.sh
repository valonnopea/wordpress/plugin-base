#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

NAME='valonnopea-plugin-base'
PREFIX='Valonnopea'
NAMESPACES_TO_FIX=( 'Carbon_Fields' )

function fix_namespaces()
{
	local namespaces="$( IFS='|' ; echo "${NAMESPACES_TO_FIX[*]}")"
	sed --regexp-extended --in-place \
		"s/^(use[[:space:]]+)(${namespaces})(\\\\)/\1${PREFIX}\\\\\2\3/g" \
		"${SOURCE_DIR}/${1}"
}

function copy_stuff()
{
	local sd="${MY_DIR}"
	local dd="${TEMPDIR}/${2}"
	rm -rf "${dd%/}/${1}"
	cp -r "${sd%/}/${1}" "${dd%/}/${1}"
}

# Get the directories to use
TEMPDIR="$(mktemp -d)"
MY_DIR="${TEMPDIR}/${NAME}.tmp"
SOURCE_DIR="${MY_DIR}/build"
TARGET_DIR="$(realpath "$(dirname "${0}")")/packages"

# Get the initial archive
git archive -o "${TEMPDIR}/${1}.zip" "${1}"

# Go to temp
pushd "${TEMPDIR}" >>'/dev/null'

# Create the directory structure expected
mkdir --parents "${MY_DIR}"

# Go to expected folder
pushd "${MY_DIR}" >>'/dev/null'

# Extract the source
unzip -qq "${TEMPDIR}/${1}.zip"

# Remove the git files
find "${MY_DIR}" -type 'f' -name '.gitignore' -delete
find "${MY_DIR}" -type 'f' -name '.gitattributes' -delete

# Install the composer dependencies
composer --no-interaction --ignore-platform-reqs \
	--working-dir="${MY_DIR}" \
	'install'

composer \
	'bin' \
	'all' 'install' \
	--no-interaction \
	--ignore-platform-reqs

# Run the scoping
"${MY_DIR}/vendor/bin/php-scoper" 'add-prefix' \
	--working-dir "${MY_DIR}" \
	--output-dir "${SOURCE_DIR}" \
	--force \
	--stop-on-failure \
	--no-interaction \
	--prefix "${PREFIX}"

# Fix the autoloader
composer --no-interaction --ignore-platform-reqs \
	--working-dir="${SOURCE_DIR}" \
	'dump-autoload' \
	--classmap-authoritative

# Fix the namespaces
fix_namespaces 'src/AbstractPlugin.php'


# Move the build dir to package dir
mv "${SOURCE_DIR}" "${TEMPDIR}/${NAME}"

# Copy the non-scoped files
copy_stuff 'loader.php' ''
copy_stuff 'valonnopea-plugin-base.php' "${NAME}"

# Remove the temporary build dir
rm -rf "${MY_DIR}"

# Back to the temp
popd >>'/dev/null'

# Remove extra files
pushd "${TEMPDIR}/${NAME}" >>'/dev/null'
rm --recursive --force \
	'.git' \
	'.gitignore' \
	'composer.json' \
	'composer.lock' \
	'install.sh' \
	'create-package.sh'
popd >>'/dev/null'

# Remove the old archive
rm "${1}.zip"

# Create the archive
zip -q -9 -r "${NAME}-${1}.zip" "${NAME}" 'loader.php'

# Back to where we were
popd >>'/dev/null'

# Copy the archive
cp "${TEMPDIR}/${NAME}-${1}.zip" "${TARGET_DIR}/${NAME}-${1}.zip"

# Remove the temp directory
rm -rf "${TEMPDIR}"
