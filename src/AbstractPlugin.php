<?php
declare(strict_types = 1);

namespace Valonnopea\WordPress;

if (!defined('ABSPATH'))
{
	exit;
}

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Container\Theme_Options_Container;
use DateTimeImmutable;
use JetBrains\PhpStorm\NoReturn;
use JsonException;
use function __;
use function admin_url;
use function debug_backtrace;
use function defined;
use function fclose;
use function fopen;
use function fwrite;
use function in_array;
use function is_admin;
use function locate_template;
use function ob_get_clean;
use function ob_start;
use function rtrim;
use function sprintf;
use function str_starts_with;
use function strtolower;
use function trailingslashit;
use function var_dump;
use function version_compare;
use function wp_doing_ajax;
use function wp_get_active_and_valid_plugins;
use function wp_get_active_network_plugins;
use function wp_get_raw_referer;

/**
 * An abstract base class for WordPress plugins
 *
 * @package Valonnopea\WordPress
 */
abstract class AbstractPlugin
{
	private const VERSION = '0.10.2';
	
	/**
	 * Variable holding the status of being in the admin backend
	 *
	 * @var bool
	 */
	protected bool $is_admin_interface;
	
	/**
	 * Variable holding the current plugin path
	 *
	 * @var string
	 */
	protected string $plugin_path;
	
	/**
	 * Variable holding the current plugin main file
	 *
	 * @var string
	 */
	protected string $plugin_file;
	
	/**
	 * Variable holding the current plugin version string
	 *
	 * @var string
	 */
	protected string $plugin_version;
	
	/**
	 * Admin menu container
	 *
	 * @var Theme_Options_Container|\WPUM\Carbon_Fields\Container\Theme_Options_Container
	 */
	protected Theme_Options_Container|\WPUM\Carbon_Fields\Container\Theme_Options_Container $container;
	
	/**
	 * Check the base plugin version
	 *
	 * @param string $version
	 *    Version to check
	 *
	 * @return bool
	 */
	public static function CheckVersion(string $version) : bool
	{
		return version_compare(self::VERSION, $version, '>=');
	}
	
	/**
	 * Check if the WooCommerce is active, including network activated
	 *
	 * @return bool
	 */
	public static function CheckWooCommerce() : bool
	{
		$plugin_path = trailingslashit(WP_PLUGIN_DIR) . 'woocommerce/woocommerce.php';
		
		return
			in_array($plugin_path, wp_get_active_and_valid_plugins(), TRUE)
			|| in_array($plugin_path, wp_get_active_network_plugins(), TRUE);
	}
	
	/**
	 * Check if WordPress User Manager is installed and enabled
	 *
	 * This is mostly because it will cause problems with Carbon Fields
	 */
	public static function CheckWPUM() : void
	{
		if (defined('VALONNOPEA_WPUM_DETECTED'))
		{
			return;
		}
		
		if (is_plugin_active('wp-user-manager/wp-user-manager.php'))
		{
			$wpum_autoload = sprintf('%s/wp-user-manager/vendor-dist/autoload.php', WP_PLUGIN_DIR);
			if (is_readable($wpum_autoload))
			{
				require_once($wpum_autoload);
				define('VALONNOPEA_WPUM_DETECTED', TRUE);
				define('VALONNOPEA_WPUM_AUTOLOAD_PATH', $wpum_autoload);
				return;
			}
		}
		define('VALONNOPEA_WPUM_DETECTED', FALSE);
	}
	
	/**
	 * Check if we have WPMU installation detected
	 *
	 * @return bool
	 */
	protected static function HasWPUM() : bool
	{
		return defined('VALONNOPEA_WPUM_DETECTED') && VALONNOPEA_WPUM_DETECTED;
	}
	
	/**
	 * Dump a variable contents to the debug log
	 *
	 * @param string $name
	 *    The variable name
	 *
	 * @param mixed $var
	 *    The variable itself
	 */
	public static function DumpVarToDebugLog(string $name, mixed $var) : void
	{
		ob_start();
		var_dump($var);
		static::WriteDebugLog(sprintf('%s: %s', $name, rtrim(ob_get_clean())));
		/*
		 *
		$caller = debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'];
		ob_start();
		var_dump($var);
		static::WriteDebugLog(sprintf('(%s::%s) %s: %s', static::class, $caller, $name, rtrim(ob_get_clean())));
		*/
	}
	
	/**
	 * Write a line to the debug log
	 *
	 * @note nothing is done if WP_DEBUG is not turned on
	 *
	 * @param string $message
	 *    The message to write
	 */
	public static function WriteDebugLog(string $message) : void
	{
		if (!defined('WP_DEBUG') || !WP_DEBUG)
		{
			return;
		}
		
		$caller = debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'];
		if ($caller === 'DumpVarToDebugLog')
		{
			$caller = debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 3)[2]['function'];
		}
		$filename = sprintf('%s/valonnopea.log', WP_CONTENT_DIR);
		$handle = fopen($filename, 'ab');
		$time = new DateTimeImmutable();
		#fwrite($handle, sprintf('[%s] %s%s', $time->format('Y-m-d H:i:s'), $message, PHP_EOL));
		fwrite($handle, sprintf('[%s] (%s::%s) %s%s', $time->format('Y-m-d H:i:s'), static::class, $caller, $message, PHP_EOL));
		fclose($handle);
	}
	
	/**
	 * Get the plugin name, used for translation namespace
	 *
	 * @return string
	 */
	abstract protected static function GetPluginName() : string;
	
	/**
	 * The constructor
	 *
	 * @param string $plugin_path
	 *    The plugin base path
	 *
	 * @param string $plugin_file
	 *    The plugin main file
	 */
	public function __construct(string $plugin_path, string $plugin_file)
	{
		$this->is_admin_interface = $this->IsAdminRequest();
		$this->plugin_path = $plugin_path;
		$this->plugin_file = $plugin_file;
		$this->plugin_version = '';
		self::CheckWPUM();
	}
	
	/**
	 * Add the generic menu
	 */
	public function Initialize() : void
	{
		// Initialize the Carbon Fields
		add_action('after_setup_theme', [$this, 'BootCarbonFields']);
		
		// Add the settings menu
		add_action('carbon_fields_register_fields', [$this, 'AddValonnopeaMenu']);
	}
	
	/**
	 * Initialize the Carbon Fields
	 */
	public function BootCarbonFields() : void
	{
		global $valonnopea_carbon_fields_booted;
		if (isset($valonnopea_carbon_fields_booted))
		{
			return;
		}
		if (!self::HasWPUM())
		{
			Carbon_Fields::boot();
		}
		$valonnopea_carbon_fields_booted = TRUE;
	}
	
	/**
	 * Get the path to the plugin directory
	 *
	 * @return string
	 */
	public function GetPluginPath() : string
	{
		return $this->plugin_path;
	}
	
	/**
	 * Get the version of the plugin
	 *
	 * @return string
	 */
	public function GetPluginVersion() : string
	{
		if (empty($this->plugin_version))
		{
			$plugin_data = get_plugin_data($this->plugin_file);
			$this->plugin_version = $plugin_data['Version'] ?? 'unknown';
		}
		
		return $this->plugin_version;
	}
	
	/**
	 * Generate the menu item
	 */
	public function AddValonnopeaMenu() : void
	{
		global $valonnopea_menu_container;
		if (isset($valonnopea_menu_container))
		{
			$this->container = $valonnopea_menu_container;
			return;
		}
		
		if (self::HasWPUM())
		{
			$this->container = \WPUM\Carbon_Fields\Container::make('theme_options', $this->__('Valonnopea'));
		}
		else
		{
			$this->container = Container::make_theme_options($this->__('Valonnopea'));
		}
		$this->container->set_page_file('valonnopea');
		$this->container->set_page_parent(FALSE);
		$this->container->set_icon('dashicons-privacy');
		$valonnopea_menu_container = $this->container;
	}
	
	/**
	 * Send out WP style AJAX data
	 *
	 * @param array $data
	 *
	 * @throws JsonException
	 *    In case of invalid JSON conversion
	 */
	#[NoReturn]
	protected function SendAJAX(array $data) : void
	{
		echo json_encode($data, JSON_THROW_ON_ERROR);
		wp_die();
	}
	
	/**
	 * Send a successful JSON response via WordPress
	 *
	 * @param mixed $data
	 *    JSON data to be sent
	 *
	 * @param int $flags
	 *    Any flags to send to json_encode
	 */
	#[NoReturn]
	protected function SendJSON(mixed $data, int $flags = JSON_THROW_ON_ERROR) : void
	{
		wp_send_json_success($data, NULL, $flags);
	}
	
	/**
	 * Send an unsuccessful JSON response via WordPress
	 *
	 * @param mixed $data
	 *    Any JSON data to be sent
	 *
	 * @param int|null $status_code
	 *    HTTP status code to send
	 *
	 * @param int $flags
	 *    Any flags to send to json_encode
	 */
	#[NoReturn]
	protected function SendError(mixed $data = NULL, ?int $status_code = NULL, int $flags = JSON_THROW_ON_ERROR) : void
	{
		wp_send_json_error($data, $status_code, $flags);
	}
	
	/**
	 * Locate the template file even if it is overridden by theme
	 *
	 * @param string $template
	 *    The template to locate
	 *
	 * @return string
	 *    The file name found
	 */
	protected function GetTemplateFileName(string $template) : string
	{
		$filename = locate_template(sprintf('%s.php', $template));
		if (!empty($filename))
		{
			return $filename;
		}
		
		return sprintf('%s/templates/%s.php', $this->GetPluginPath(), $template);
	}
	
	/**
	 * Generate a component name for Carbon fields
	 *
	 * @param string $component
	 *    Component name to generate
	 *
	 * @return string
	 *    Component name prefixed with plugin name to make it more unique
	 */
	protected function CreateComponentName(string $component) : string
	{
		return sprintf('%s-%s', static::GetPluginName(), $component);
	}
	
	/**
	 * Get the name for the input field on the HTML form
	 *
	 * @return string
	 */
	protected function GetFormFieldName() : string
	{
		return sprintf('%s-hidden-ids', static::GetPluginName());
	}
	
	/**
	 * Translate a message using plugin name space
	 *
	 * @param string $text
	 *    Text to translate
	 *
	 * @return string
	 *    Translated text
	 */
	protected function __(string $text) : string
	{
		return __($text, static::GetPluginName());
	}
	
	/**
	 * Remove all the cookies containing the current plugin name
	 */
	protected function RemoveMyCookies() : void
	{
		if (!isset($_SERVER['HTTP_COOKIE']))
		{
			return;
		}
		
		$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
		foreach ($cookies as $cookie)
		{
			[$name,] = explode('=', $cookie, 2);
			$name = trim($name);
			if (str_contains($name, static::GetPluginName()))
			{
				setcookie($name, '', time() - 3600);
				setcookie($name, '', time() - 3600, '/');
			}
		}
	}
	
	/**
	 * Try some URL magic to determine if a request is made from frontend or admin backend
	 *
	 * @return bool
	 */
	protected function IsAdminRequest() : bool
	{
		if (is_admin() === TRUE && wp_doing_ajax() === FALSE)
		{
			return TRUE;
		}
		
		/**
		 * Get admin URL and referrer
		 *
		 * @link https://core.trac.wordpress.org/browser/tags/4.8/src/wp-includes/pluggable.php#L1076
		 */
		$admin_url = strtolower(admin_url());
		$referrer = wp_get_raw_referer();
		if ($referrer)
		{
			$referrer = strtolower($referrer);
		}
		else
		{
			$referrer = '';
		}
		
		/**
		 * Check if the user comes from an admin page.
		 */
		if (str_starts_with($referrer, $admin_url))
		{
			return TRUE;
		}
		
		return FALSE;
	}
}
